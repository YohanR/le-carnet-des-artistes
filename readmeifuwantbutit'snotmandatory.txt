lien du site en ligne : http://yohanr-carnet.herokuapp.com/
user : test@test    testtest
admin : admin@admin adminadmin
lien du gitlab : https://gitlab.com/YohanR/le-carnet-des-artistes
trello : https://trello.com/b/b8UnB2mR/le-carnet-des-artistes

Contexte du projet

La "société des beaux-parleurs" est un club d'improvisation théatrale, dont les membres se réunissent chaque jeudi soir pour s'adonner à leur pratique préférée. L'impro théatrale se déroule comme suit, sous forme de "battles" : l'arbitre au centre, et deux équipes de 2 ou 3 personnes de chaque côté. L'arbitre annonce un sujet, par exemple :

    Deux familles se croisent au concours de barbecue de Dunkerque

Les deux teams commencent alors à improviser. Parfois, l'arbitre mentionne une contrainte supplémentaire : l'impro se déroule en language "gromlo".

À la fin du battle, une équipe est déclarée gagnante par l'arbitre, qui rend son verdict à l'applaudimètre : en invitant le public à faire un maximum de bruit pour l'équipe qui l'a le plus convaincu !

L'association vous sollicite pour créer un outil en ligne, qui permette à l'ensemble des membres du club de proposer des sujets durant la semaine.

Une page "roulette" permettrait ensuite, le jour J, de tirer au sort les sujets au fil de la soirée (à chaque chargement de page, un sujet aléatoire est affiché).

Petite précision : lorsqu'un sujet s'affiche, il devrait être marqué comme "traité" et ne plus être affiché par la suite !

Il est à noter que l'association dispose d'un hébergement mutualisé chez OVH (offre premier prix) ainsi que d'un nom de domaine.

Facultativement, l'appli pourrait permettre au président du club de saisir les prochains lieux et dates et , ainsi que la liste des adhérents. Tout cela serait donc visible sur le site.
Conseil

Ne passez surtout pas plus de 3 heures sur le front (design, HTML, CSS), ce n'est pas l'objet de ce brief !
Modalités pédagogiques

Travail individuel. Vous avez 3 jours, à compter du jour d'assignation du brief.
Critères de performance

    Compétence 3 : Les traitements attendus côté client sont effectués
    Compétence 5 : Un script de création de base de données est mis en place
    Compétence 6 : Les composants d'accès aux données (models) sont en place
    Compétence 7 : Le backend est fonctionnel (routes, controlleurs, vues...)

Modalités d'évaluation

Ne seront évalués que les projets répondant aux attentes suivantes :

    respect de la deadline (avant minuit du dernier jour, l'heure de Simplonline faisant foi)
    dépôt Gitlab accessible (merci de bien vérifier ce point)
    plus de 3 commits légitimes par jour
    un fichier README.md clair permet d'accéder facilement aux différentes ressources

Livrables

L'ensemble des livrables doit être présent dans un dépôt GIT :

- le code source, réalisé avec un framework adapté
- le lien vers une version fonctionnelle en ligne (hébergé sur Heroku par exemple)
- le dossier de conception (MCD, diagramme de séquence, cas d'utilisation...)
- le lien vers le kanban (Trello, Asana...)
