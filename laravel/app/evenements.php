<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class evenements extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date_debut', 'date_fin','lieu',
    ];
    }
