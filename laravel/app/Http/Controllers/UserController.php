<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\User;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class UserController extends Controller

{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       if ($this->authorize('user_list')){
        $user = DB::table('users')->get();
  
        $value = json_decode($user, true);
        $nb_users = \DB::table('users')->count();
        $nb_users = json_encode($nb_users);
            
            $count = $nb_users;


        foreach( $value as $users ){

            array_chunk($users, $count, true);
         }

        return view('adherents')->with('users', $value);
    }else{
        return view('notadmin');
    }
        //
    }
}
 
