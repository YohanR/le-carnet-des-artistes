<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\evenements;    
class validatedateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function date()
    {
        $datedebut = $_GET['datedebut'];
        $datefin = $_GET['datefin'];   
        $lieu = $_GET['lieu'];  
        $evenements = new evenements;
        $evenements-> date_debut = $datedebut;
        $evenements-> date_fin = $datefin;
        $evenements-> lieu = $lieu;
        $evenements->save();
        return view('validatedate');
    }


}
