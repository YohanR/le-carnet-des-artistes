@extends('layouts.app')

@section('content')
<div class = "container-fluid bg-info pt-3 pb-3">
<h1 class = "text-center">vous n'avez pas les droits pour accéder a cette page</h1>
<div class = "d-flex row ">

    <form class ="w-100" action="/addsujet" method="GET">
        <div class = "col-sm-10 col-12 ">
            <input class="form-control" name="addtitresujet" type="search" placeholder="Titre de votre sujet" aria-label="Search">
         </div>
         <div class = "col-sm-10 col-12 ">
            <input class="form-control" name="adddescriptionsujet" type="search" placeholder="Description de votre sujet" aria-label="Search">
         </div>
         <div class = "col-sm-2 col-12 ">
            <button class=" mt-3 btn btn-outline-success bg-success text-light" type="submit">Envoyer votre sujet</button>
        </div>
        </form>
</div>
<div class="container-fluid pt-3">
<h1 class="text-center">Le prochain événement aura lieu le {{$data->date_debut}} à {{$data->date_fin}}              .</h1>
</div>
<form class = " pb-3 text-center" action="changedate" method="GET">
       <div class = "col-12 text-center ">
          <button class="btn btn-outline-success bg-success text-light "  type="submit">changer la date</button>
      </div>
      </form>

@endsection
