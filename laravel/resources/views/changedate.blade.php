@extends('layouts.app')

@section('content')
<div class = "container-fluid bg-info pt-3 pb-3">
<div class = "d-flex row ">
<form class ="w-100" action="/validatedate" method="GET">
        <div class = "col-sm-10 col-12 ">
        <label for="date de début">Date de début</label>
            <input class="form-control" name="datedebut" type="date"  min="{{ date('Y-m-d') }}"  placeholder=`{{$data->date_debut}}` aria-label="Search">
         </div>
         <div class = "col-sm-10 col-12 ">
         <label for="date de fin">Date de fin</label>
            <input class="form-control" name="datefin" type="date"  min="{{ date('Y-m-d') }}"  placeholder=`{{$data->date_fin}}` aria-label="Search">
         </div>
         <div class = "col-sm-10 col-12 ">
         <label for="date de fin">lieu</label>
            <input class="form-control" name="lieu" type="texte" required placeholder="{{$data->lieu}}" aria-label="Search">
         </div>
         <div class = "col-sm-2 col-12 ">
            <button class=" mt-3 btn btn-outline-success bg-success text-light" type="submit">Ajouter la date du prochain event</button>
        </div>
        
        </form>
    

@endsection
