@extends('layouts.app')

@section('content')
<div class = "container-fluid bg-info pt-3 pb-3">
<div class = "d-flex row ">

    <form class ="w-100" action="/addsujet" method="GET">
        <div class = "col-sm-10 col-12 ">
            <input class="form-control" name="addtitresujet" type="search" required placeholder="Titre de votre sujet" aria-label="Search">
         </div>
         <div class = "col-sm-10 col-12 ">
            <input class="form-control" name="adddescriptionsujet" type="search" required placeholder="Description de votre sujet" aria-label="Search">
         </div>
         <div class = "col-sm-2 col-12 ">
            <button class=" mt-3 btn btn-outline-success bg-success text-light" type="submit">Envoyer votre sujet</button>
        </div>
        </form>
</div>
{{--<?php $dateDb = $data->date_debut;
        setlocale(LC_TIME, 'french.UTF-8', 'fr_FR.UTF-8');

        $dateFr = strftime("%A %d %B %G", strtotime($dateDb)); 
         $dateDb2 = $data->date_fin;
        setlocale(LC_TIME, 'french.UTF-8', 'fr_FR.UTF-8');

        $dateFr2 = strftime("%A %d %B %G", strtotime($dateDb2)); ?> --}}
<div class="container-fluid pt-3">
<h1 class="text-center">Le prochain événement aura lieu le {{$data->date_debut}} au {{$data->date_fin}} à {{$data->lieu}}.</h1>
</div>
@can('user_list')
<form class = " pb-3 text-center" action="changedate" method="GET">
       <div class = "col-12 text-center ">
          <button class="btn btn-outline-success bg-success text-light "  type="submit">changer la date</button>
      </div>
      </form>
@endcan
@endsection
