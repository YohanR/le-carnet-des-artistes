@extends('layouts.app')

@section('content')

<div class = "container-fluid bg-info pt-3 pb-3">
<div class = "d-flex row ">

    <form class ="w-100" action="/addsujet" method="GET">
        <div class = "col-sm-10 col-12">
            <input class="form-control" name="addtitresujet" type="search" required placeholder="Titre de votre sujet" aria-label="Search">
         </div>
         <div class = "col-sm-10 col-12">
            <input class="form-control" name="adddescriptionsujet" type="search" required placeholder="Description de votre sujet" aria-label="Search">
         </div>
         <div class = "col-sm-2 col-12 ">
            <button class=" mt-3 btn btn-outline-success bg-success text-light" type="submit">Envoyer votre sujet</button>
        </div>
        </form>
        <div class = "container-fluid text-center"><p>Votre sujet a bien été ajouté</p></div>
</div>
@endsection
