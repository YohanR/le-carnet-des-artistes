<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/home','HomeController@index', function () {
    return view('index');
});
Auth::routes();
Route::get('/','HomeController@index');
Route::get('/addsujet','sujetsController@addsujets');

Route::get('/roulette','rouletteController@roulette');
Route::resource('adherents', 'UserController');
Route::get('/deletedb','deletedbController@deletedb');
Route::get('/changedate','changedateController@date');

Route::get('/validatedate','validatedateController@date');
Route::get('/notadmin','HomeController@index', function () {
    return view('notadmin');
});